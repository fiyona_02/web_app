const express = require('express');
const app = express();
const port = 3000;

// Set up body parser to handle form data
app.use(express.urlencoded({ extended: true }));

// Serve static files from the public directory
app.use(express.static('public'));

// Home page route
app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

// Form submission route
app.post('/submit', (req, res) => {
  const { name, email } = req.body;
  // Here you can do whatever you want with the submitted data
  console.log(`Received form submission: Name - ${name}, Email - ${email}`);
  res.send('Form submitted successfully!');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is listening at http://localhost:${port}`);
});