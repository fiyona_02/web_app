Step 1: Create .gitlab-ci.yml file: Add a .gitlab-ci.yml file to the root directory of your repository.
Step 2  Commit and Push Changes: Commit the .gitlab-ci.yml file to your repository and push the changes to GitLab.
Step 3: Configure GitLab CI/CD Settings: Go to your project's settings in GitLab, navigate to CI/CD, and ensure that CI/CD pipelines are enabled.
Step 4: Run CI/CD Pipeline: GitLab will automatically trigger the CI/CD pipeline whenever you push changes to your repository. Monitor the pipeline's progress in the GitLab UI.
